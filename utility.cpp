#include "utility.h"

using namespace std;

int EditPrintf(HWND hwndEdit, TCHAR * szFormat, ...)
{
	TCHAR   szBuffer[MAXLINE];
	va_list pArgList;

	va_start(pArgList, szFormat);
	wvsprintf(szBuffer, szFormat, pArgList);
	va_end(pArgList);

	SendMessage(hwndEdit, EM_SETSEL, (WPARAM)-1, (LPARAM)-1);
	SendMessage(hwndEdit, EM_REPLACESEL, FALSE, (LPARAM)szBuffer);
	SendMessage(hwndEdit, EM_SCROLLCARET, 0, 0);
	return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0);
}

string replace_all(string str, const string& from, const string& to) {
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length();
	}
	return str;
}

void htmloutput(SOCKET sock, string msg, int id, bool is_command) {
	msg = replace_all(msg, "<", "&lt;");
	msg = replace_all(msg, ">", "&gt;");
	msg = replace_all(msg, "\n", "<br>");
	msg = replace_all(msg, "\r", "");
	msg = replace_all(msg, "\"", "&quot;");
	if (is_command) {
		msg = "<b>" + msg + "</b>";
	}
	// replace <, > by &lt, &gt
	string str = "<script>document.getElementById(\"" + to_string(id) + "\")";
	writen(sock, str.c_str(), str.length());
	//cout << "<script>document.getElementById(\"" << id << "\")";
	str = ".innerHTML += \"" + msg + "\";</script>";
	writen(sock, str.c_str(), str.length());
	//cout << ".innerHTML += \"" << msg << "\";</script>";
}

template<typename Out>
void _split(const string &s, char delim, Out result) {
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		*(result++) = item;
	}
}

vector<string> split(const string &s, char delim) {
	vector<string> elems;
	_split(s, delim, back_inserter(elems));
	return elems;
}

int readn(SOCKET sock, char *vptr, size_t n) {
	size_t nleft;
	int nread;
	char *ptr;

	ptr = vptr;
	nleft = n;
	while (nleft > 0) {
		if ((nread = recv(sock, ptr, nleft, 0)) < 0) {
			int err = WSAGetLastError();
			if (err == WSAEINTR) {
				nread = 0;
			}
			else if (err == WSAEWOULDBLOCK) {
				return n - nleft;
			}
			else {
				return -1;
			}
		}
		else if (nread == 0)
			break;
		nleft -= nread;
		ptr += nread;
	}
	return n - nleft;
}

int writen(SOCKET sock, const char *vptr, size_t n) {
	size_t left = n;
	int written;
	const char *ptr = vptr;
	while (left > 0) {
		if ((written = send(sock, ptr, left, 0)) <= 0) {
			if (written < 0 && WSAGetLastError() == WSAEINTR) {
				written = 0;
			}
			else {
				return -1;
			}
		}

		left -= written;
		ptr += written;
	}
	return n;
}
