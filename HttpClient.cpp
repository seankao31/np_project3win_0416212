#include <Windows.h>
#include "HttpClient.h"

using namespace std;

HttpClient::HttpClient()
{
}

HttpClient::~HttpClient()
{
}

void HttpClient::parse_request() {
	vector<string> lines = split(request, '\n');
	for (size_t i = 0; i < lines.size(); ++i) {
		EditPrintf(hwnd, TEXT("line: %s\r\n"), lines[i].c_str());
	}
	requestline = lines[0];
	vector<string> words = split(requestline, ' ');
	for (size_t i = 0; i < words.size(); ++i) {
		EditPrintf(hwnd, TEXT("word: %s\r\n"), words[i].c_str());
	}
	method = words[0];
	request_version = words[2];
	vector<string> wwords = split(words[1], '?');
	path = wwords[0].substr(1);
	if (wwords.size() > 1)
		query_string = wwords[1];
	EditPrintf(hwnd, TEXT("method: %s\r\n"), method.c_str());
	EditPrintf(hwnd, TEXT("path: %s\r\n"), path.c_str());
	EditPrintf(hwnd, TEXT("query_string: %s\r\n"), query_string.c_str());
	EditPrintf(hwnd, TEXT("request_version: %s\r\n"), request_version.c_str());
}

void HttpClient::send_response(int status) {
	string msg;
	switch (status) {
	case 200:
		msg = "HTTP/1.1 200 OK\r\n";
		//send(sock, msg.c_str(), msg.length(), 0);
		writen(sock, msg.c_str(), msg.length());
		break;
	}
}

void HttpClient::send_header() {
	string msg = "Content-Type: text/html\r\n";
	//send(sock, msg.c_str(), msg.length(), 0);
	//send(sock, "\r\n", 2, 0);
	writen(sock, msg.c_str(), msg.length());
	writen(sock, "\r\n", 2);
}

void HttpClient::do_GET() {
	size_t pos = path.find(".cgi");
	EditPrintf(hwnd, TEXT("pos = %u"), pos);
	if (pos == string::npos || pos != path.length() - 4) {
		forward_data();
	}
	else {
		execute_CGI();
	}
}

void HttpClient::forward_data() {
	send_response(200);
	send_header();
	FILE *fin;
	fopen_s(&fin, path.c_str(), "r");
	char readbuf[MAXLINE];
	while (fgets(readbuf, MAXLINE, fin) != NULL) {
		EditPrintf(hwnd, TEXT("read from file: %s\r\n"), readbuf);
		//send(sock, readbuf, strlen(readbuf), 0);
		writen(sock, readbuf, strlen(readbuf));
	}
	fclose(fin);
}

void HttpClient::execute_CGI() {
	cgi.hwnd = hwnd;
	cgi.sock = sock;
	cgi.query = query_string;
	cgi.parse_query();
	cgi.add_clients();
	send_response(200);
	cgi.execute();
}