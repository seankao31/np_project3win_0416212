#pragma once
#include <windows.h>
#include <string>
#include <vector>
#include "utility.h"
#include "CGI.h"

class HttpClient
{
public:
	HWND hwnd;
	SOCKET sock;
	std::string request;
	std::string requestline;
	std::string method;
	std::string path;
	std::string query_string;
	std::string request_version;
	CGI cgi;

public:
	HttpClient();
	~HttpClient();

	void parse_request();
	void send_response(int);
	void send_header();
	void do_GET();
	void forward_data();
	void execute_CGI();
};

