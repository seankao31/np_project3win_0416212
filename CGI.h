#pragma once
#include <string>
#include <vector>
#include <utility>

#include "utility.h"

#define WM_CGI_SOCKET_NOTIFY (WM_USER + 2)

#define NOTWRITABLE 1
#define WRITABLE 2

class CGI
{
public:
	class Client {
	public:
		std::string host, port, file;
		int id;
		SOCKET sock;
		int status;
		struct sockaddr_in sin;
		FILE *fin;
	};

public:
	HWND hwnd;
	SOCKET sock;
	std::string query;
	std::vector<Client> clients;

public:
	CGI();
	~CGI();
	void execute();
	void parse_query();
	void add_clients();
	void html_init_output();
};

