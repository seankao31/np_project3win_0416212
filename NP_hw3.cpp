#include <windows.h>
#include <string>
#include <vector>
#include <list>
using namespace std;

#include "resource.h"
#include "utility.h"
#include "HttpClient.h"
#include "CGI.h"

#define SERVER_PORT 7799

#define WM_SOCKET_NOTIFY (WM_USER + 1)

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
//=================================================================
//	Global Variables
//=================================================================
list<SOCKET> Socks;
vector<HttpClient> Hclients;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;

	HttpClient hclient;
	char readbuf[MAXLINE];
	int len;
	size_t i, j;

	int err;


	switch(Message) 
	{
		case WM_INITDIALOG:
			hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_LISTEN:

					WSAStartup(MAKEWORD(2, 0), &wsaData);

					//create master socket
					msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					if( msock == INVALID_SOCKET ) {
						EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
						WSACleanup();
						return TRUE;
					}

					err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);

					if ( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
						closesocket(msock);
						WSACleanup();
						return TRUE;
					}

					//fill the address info about server
					sa.sin_family		= AF_INET;
					sa.sin_port			= htons(SERVER_PORT);
					sa.sin_addr.s_addr	= INADDR_ANY;

					//bind socket
					err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
						WSACleanup();
						return FALSE;
					}

					err = listen(msock, 2);
		
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
						WSACleanup();
						return FALSE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
					}

					break;
				case ID_EXIT:
					EndDialog(hwnd, 0);
					break;
			};
			break;

		case WM_CLOSE:
			EndDialog(hwnd, 0);
			break;

		case WM_SOCKET_NOTIFY:
			switch( WSAGETSELECTEVENT(lParam) )
			{
				case FD_ACCEPT:
					ssock = accept(msock, NULL, NULL);
					Socks.push_back(ssock);
					EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, Socks.size());
					hclient.hwnd = hwndEdit;
					hclient.sock = ssock;
					Hclients.push_back(hclient);
					break;
				case FD_READ:
				//Write your code for read event here.
					for (i = 0; i < Hclients.size(); ++i) {
						if (Hclients[i].sock == wParam)
							break;
					}
					len = recv(Hclients[i].sock, readbuf, sizeof(readbuf), 0);
					if (len <= 0) {
						if (WSAGetLastError() != WSAEINTR) {
							// error handle
						}
						break;
					}
					readbuf[len] = '\0';
					EditPrintf(Hclients[i].hwnd, TEXT("Received: %s\r\n"), readbuf);
					Hclients[i].request += readbuf;
					if (Hclients[i].request.find("\r\n\r\n") || Hclients[i].request.find("\n\n")) {
						Hclients[i].parse_request();
						Hclients[i].do_GET();
						// HERE?
						for (auto &cli : Hclients[i].cgi.clients) {
							int err = WSAAsyncSelect(cli.sock, hwnd, WM_CGI_SOCKET_NOTIFY, FD_READ | FD_WRITE);

							if (err == SOCKET_ERROR) {
								EditPrintf(hwnd, TEXT("=== Error: select error ===\r\n"));
								closesocket(cli.sock);
							}
						}

					}

					break;
				case FD_WRITE:
				//Write your code for write event here

					break;
				case FD_CLOSE:
					break;
			};
			break;
		
		case WM_CGI_SOCKET_NOTIFY:
			switch (WSAGETSELECTEVENT(lParam))
			{
			case FD_READ:
				//Write your code for read event here.
				for (i = 0; i < Hclients.size(); ++i) {
					for (j = 0; j < Hclients[i].cgi.clients.size(); ++j) {
						if (Hclients[i].cgi.clients[j].sock == wParam)
							goto FOUND;
					}
				}
				FOUND:

				// read
				
				if ((len = readn(Hclients[i].cgi.clients[j].sock, readbuf, MAXLINE)) < 0) {
					EditPrintf(Hclients[i].cgi.hwnd, TEXT("===Read Error===\r\n"));
				}
				else if (len == 0) {
					EditPrintf(Hclients[i].cgi.hwnd, TEXT("Client %d exited\r\n"), Hclients[i].cgi.clients[j].id);
					closesocket(Hclients[i].cgi.clients[j].sock);
					fclose(Hclients[i].cgi.clients[j].fin);
				}
				else {
					readbuf[len] = '\0';
					EditPrintf(Hclients[i].cgi.hwnd, TEXT("Received: %s\r\n"), readbuf);
					htmloutput(Hclients[i].cgi.sock, readbuf, Hclients[i].cgi.clients[j].id, false);
					if (string(readbuf).find("% ") != string::npos) {
						Hclients[i].cgi.clients[j].status = WRITABLE;
						EditPrintf(Hclients[i].hwnd, TEXT("WRITABLE\r\n"));
					}
				}

				// write
				if (Hclients[i].cgi.clients[j].status == WRITABLE) {
					char cmdbuff[MAXLINE];
					if (fgets(cmdbuff, MAXLINE, Hclients[i].cgi.clients[j].fin) == NULL && feof(Hclients[i].cgi.clients[j].fin)) {
						writen(Hclients[i].cgi.clients[j].sock, "exit\n", 5);
					}
					else {
						writen(Hclients[i].cgi.clients[j].sock, cmdbuff, strlen(cmdbuff));
						htmloutput(Hclients[i].cgi.sock, string(cmdbuff), Hclients[i].cgi.clients[j].id, true);
						EditPrintf(Hclients[i].hwnd, TEXT("Command: %s\r\n"), cmdbuff);
					}
					Hclients[i].cgi.clients[j].status = NOTWRITABLE;
				}


				break;
			case FD_WRITE:
				//Write your code for write event here

				break;
			};
			break;

		default:
			return FALSE;


	};

	return TRUE;
}
