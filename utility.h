#pragma once
#include <windows.h>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>

#define MAXLINE 10001

int EditPrintf(HWND, TCHAR *, ...);
std::string replace_all(std::string, const std::string&, const std::string&);
void htmloutput(SOCKET, std::string, int, bool);
template<typename Out>
void _split(const std::string&, char, Out);
std::vector<std::string> split(const std::string &, char);
int readn(SOCKET, char*, size_t);
int writen(SOCKET, const char*, size_t);
