#include <windows.h>
#include "CGI.h"

using namespace std;

CGI::CGI()
{
}

CGI::~CGI()
{
}

void CGI::execute() {
	html_init_output();
	for (auto &cli : clients) {
		/*
		int err = WSAAsyncSelect(cli.sock, hwnd, WM_CGI_SOCKET_NOTIFY, FD_CONNECT | FD_READ | FD_WRITE);

		if (err == SOCKET_ERROR) {
			EditPrintf(hwnd, TEXT("=== Error: select error ===\r\n"));
			closesocket(cli.sock);
		}
		*/

		EditPrintf(hwnd, TEXT("try connect %d\r\n"), cli.id);
		connect(cli.sock, (struct sockaddr *) &cli.sin, sizeof(cli.sin));
	}
}

void CGI::parse_query() {
	vector<pair<string, string> > parsed_query;
	int lpos = 0, rpos;
	string name, value;
	query += "&";
	while ((rpos = query.find("=", lpos)) != string::npos) {
		name = query.substr(lpos, rpos - lpos);
		lpos = rpos + 1;
		rpos = query.find("&", lpos);
		value = query.substr(lpos, rpos - lpos);
		lpos = rpos + 1;
		parsed_query.push_back({ name, value });
	}
	for (auto pq : parsed_query) {
		EditPrintf(hwnd, TEXT("parsed query[%s]: %s\r\n"), pq.first.c_str(), pq.second.c_str());
	}

	int id = 0;
	for (int i = 0; i < 5; ++i) {
		Client cli;
		cli.host = parsed_query[i * 3].second;
		cli.port = parsed_query[i * 3 + 1].second;
		cli.file = parsed_query[i * 3 + 2].second;
		if (!cli.host.empty() && !cli.port.empty() && !cli.file.empty()) {
			cli.id = id++;
			clients.push_back(cli);
		}
	}
	for (auto cli : clients) {
		EditPrintf(hwnd, TEXT("client host: %s\r\n"), cli.host.c_str());
		EditPrintf(hwnd, TEXT("client port: %s\r\n"), cli.port.c_str());
		EditPrintf(hwnd, TEXT("client file: %s\r\n"), cli.file.c_str());
	}
}

void CGI::add_clients() {
	for (auto &cli : clients) {
		struct hostent *phe;
		memset((char*)&cli.sin, 0, sizeof(cli.sin));
		cli.sin.sin_family = AF_INET;
		cli.sin.sin_port = htons((u_short)atoi(cli.port.c_str()));
		if ((phe = gethostbyname(cli.host.c_str())))
			cli.sin.sin_addr = *((struct in_addr *) phe->h_addr_list[0]);
		else
			cli.sin.sin_addr.s_addr = inet_addr(cli.host.c_str());
		cli.sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
		fopen_s(&cli.fin, cli.file.c_str(), "r");
		//FD_SET(fileno(cli.fin), &afds);
		cli.status = NOTWRITABLE;
	}
}

void CGI::html_init_output() {
	string output = "Content-Type: text/html\r\n\r\n";
	output += "<html>\n \
             <head>\n \
             <meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\n \
             <title>Network Programming Homework 3</title>\n \
             </head>\n \
             <body bgcolor=#336699>\n \
             <font face=\"Courier New\" size=2 color=#FFFF99>\n \
             <table width=\"800\" border=\"1\">\n \
             <tr>\n";
	for (auto cli : clients) {
		output += "<td>" + cli.host + "</td>\n";
	}
	output += "</tr>\n<tr>";
	for (int i = 0; i < clients.size(); ++i) {
		output += "<td valign=\"top\" id=\"" + to_string(i) + "\"></td>\n";
	}
	output += "</tr>\n";
	output += "</table>\n";
	output += "</font>\n</body>\n</html>";
	//send(sock, output.c_str(), output.length(), 0);
	writen(sock, output.c_str(), output.length());
}